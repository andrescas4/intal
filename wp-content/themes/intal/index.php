<?php get_header(); ?>

<div class="container">
	<div class="">
	<div class="grid">
	<div class="grid-sizer"></div>
		<?php
		if ( have_posts() ) : ?>
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile;
			the_posts_navigation();
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>
	</div>	
	</div>
</div>

<?php get_footer();
