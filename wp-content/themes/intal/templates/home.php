<?php 
/***
 * template name: Inicio
 */
 get_header();
?>
<div class="container">
    <div class="row cursos">
        <div class="col-xl-12">
            <h1 class="title title--home"><?php echo get_post_meta($post->ID, 'home_proximos_cursos_titulo', true); ?></h1>
        </div>
        <?php get_template_part('template-parts/query', 'cursos'); ?>
    </div>
    <div class="row buttonsWrapper--home">
        <div class="col-xl-12 text-center">
        <a href="<?php echo home_url('/cursos')?>" class="button button--read-more button--center">Más cursos</a>
        </div>
    </div>
</div>

<div class="valores valores--bg-color">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1 class="title title--home"><?php echo get_post_meta($post->ID, 'home_valores_titulo', true); ?></h1>
            </div>
            <?php get_template_part('template-parts/query', 'valoreshome');?>
        </div>
    </div>
</div>

<div class="container--centered">
    <p class="certificados text-center"><?php get_template_part('template-parts/query', 'certificados'); ?></p>
</div>

<div class="servicios servicios--bg-color">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1 class="title title--home">Servicios</h1>
            </div>
            <?php get_template_part('template-parts/query', 'servicios');?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row blogHome">
        <div class="col-xl-12">
            <h1 class="title title--home">Banco del conocimiento</h1>
        </div>
        <?php get_template_part('template-parts/query', 'postshome'); ?>
    </div>
    <div class="row buttonsWrapper--home">
        <div class="col-xl-12 text-center">
            <a href="<?php echo home_url('/banco-de-conocimiento')?>" class="button button--read-more button--center">Ver más</a>
        </div>
    </div>
</div>
<?php get_footer();?>