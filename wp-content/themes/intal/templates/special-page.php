<?php 
/*
*   template name: Plantilla con extras
*/
get_header();
?>
<?php global $post;?>
<div class="serviciosBanner d-flex justify-content-center align-items-center" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID) ?>)">
	<h1 class="title"><?php   echo get_the_title($post->ID);?></h1>
</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page-special' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
		</div><!-- #main -->
    </div><!-- #primary -->
    <div class="objetivos objetivos--bg-color">
                <div class="container">
                    <div class="row">
                        <div class="col-xl">
                            <?php echo wpautop( get_post_meta( $post->ID, 'page_listado', true ) ); ?>
                        </div>
                    </div>
                </div>
    </div>
<?php get_footer();?>
