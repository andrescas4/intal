(function($){
$(document).ready(function(){

  var mmenu = $("#menuRes").mmenu({}).data('mmenu');
  $('#btnMenuRes').click(function(ev) {
    ev.preventDefault();
    mmenu.open();
  });
    $('.grid').imagesLoaded().done( function( instance ) {
        console.log('Imágenes encontradas =)');
        $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                columnWidth: '.grid-sizer'
              }
        });
      });

      var $sliders = $(".empleadosSlide");
      var $arrows = $('.arrows');
      
      $('.equipo-bg').imagesLoaded().done(function(){
        $(".empleados-slick").each(function(){
            
            var $this = $(this);
            var slick = $this.find( $sliders ).slick({
                appendArrows: $this.find( $arrows ),
                prevArrow: '<a><i class="fa fa-chevron-left" aria-hidden="true"></i></a>',
                nextArrow: '<a><i class="fa fa-chevron-right" aria-hidden="true"></i></a>',
                arrows:true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false,
                responsive: [
                    {
                      breakpoint: 992,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: false,
                        dots: false
                      }
                    },
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                      }
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                      }
                    }
                  ]
            });
    
        });
      })
      


      $('.s').popover()
})
})(jQuery)