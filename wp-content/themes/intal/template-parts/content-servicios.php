<div class="col-xl-3">
	<article <?php post_class(); ?>>
		<div class="entradasServicios">
			<a href="<?php the_permalink(); ?>">
				<div class="imgWrapper">
					<?php 
						if ( has_post_thumbnail() ) { 
							the_post_thumbnail( 'posts-medium', array('class' => 'img-responsive')); 
						}
					?>
				</div>
				<div class="entradasServicios__info">
					<h2 class="title title--servicio"><?php the_title();?></h2>
					<?php the_excerpt();?>
				</div>
			</a>
		</div>
	</article><!-- #post -->
</div>