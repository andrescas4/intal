<?php 
global $post;

$certificadosArg = array(
    'post_type' => 'certificados',
    'posts_per_page' => -1,
    'post_status' => 'publish'
);

$certificados = new WP_Query($certificadosArg);
?>

<?php if($certificados->have_posts()) :?>
<?php while($certificados->have_posts()) : $certificados->the_post(); ?>
    <?php echo get_the_post_thumbnail(get_the_id(), 'full'); ?>
<?php endwhile;?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
	<p><?php esc_html_e( 'Lo sentimos, no hay certificados para mostrar.' ); ?></p>
<?php endif; ?>