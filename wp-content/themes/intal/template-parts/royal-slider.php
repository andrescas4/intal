
<?php
if(is_front_page()){
    echo do_shortcode('[rev_slider alias="homeslider"]'); 
}

if(is_post_type_archive('servicios')){
    echo do_shortcode('[rev_slider alias="slider-servicios"]'); 
}

if(is_post_type_archive('cursos')){
    echo do_shortcode('[rev_slider alias="slidercursos"]'); 
}
?>