<?php 
global $post;

$valoresArg = array(
    'post_type' => 'valores',
    'posts_per_page' => -1,
    'post_status' => 'publish'
);

$valores = new WP_Query($valoresArg);
?>

<?php if($valores->have_posts()) :?>
<?php while($valores->have_posts()) : $valores->the_post(); ?>
    <div class="col-lg-4">
        <aside class="valor">
            <div class="d-flex justify-content-center">
                <?php 
                    if ( has_post_thumbnail() ) { 
                        the_post_thumbnail( 'full', array('class' => 'img-responsive')); 
                    }
                ?>
            </div>   
            <h2 class="title title--valores"><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </aside>
    </div>
<?php endwhile;?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
	<p><?php esc_html_e( 'Lo sentimos, no hay entradas para mostrar.' ); ?></p>
<?php endif; ?>