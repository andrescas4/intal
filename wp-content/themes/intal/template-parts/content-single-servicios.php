<div class="col-xl-9 col-lg-9 col-md-9">
		<article id="post-<?php the_ID(); ?>" <?php post_class('entradasInternas entradasCursos'); ?>>
			<div class="entry-content">
			
				<?php
					the_content();	

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'intal' ),
						'after'  => '</div>',
					) );
				?>		
			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->
</div>
<div class="col-xl-3 col-lg-3 col-md-3">
	<?php get_sidebar('servicios'); ?>
</div>
