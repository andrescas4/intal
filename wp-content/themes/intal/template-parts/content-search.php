<li class="list-group-item">
	<article class="searchResult">
		<a href="<?php the_permalink()?>">
			<header class="entry-header">
				<h1 class="title title--blog"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->	
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
		</a>
	</article>
</li>