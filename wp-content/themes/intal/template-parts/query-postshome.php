<?php 
global $post;

$postsArg = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
    'post_status' => 'publish'
);

$posts = new WP_Query($postsArg);
?>

<?php if($posts->have_posts()) :?>
<?php while($posts->have_posts()) : $posts->the_post(); ?>
    <div class="col-lg-4">
        <article class="entradas">
            <a href="<?php the_permalink(); ?>">
            <div class="postImgHolder" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'posts-regular')?>)"></div>
                <div class="entradas__info">
                    <h2 class="title title--blog"><?php the_title();?></h2>
                    <?php the_excerpt();?>
                </div>
            </a>
        </article>
    </div>
<?php endwhile;?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
	<p><?php esc_html_e( 'Lo sentimos, no hay entradas para mostrar.' ); ?></p>
<?php endif; ?>