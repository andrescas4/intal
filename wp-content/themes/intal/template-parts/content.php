<?php if(is_important(get_the_ID())) :?>
	<article <?php post_class('grid-item important'); ?>>
<?php else :?>
	<article <?php post_class('grid-item'); ?>>
<?php endif; ?>

	<div class="entradas entradas--masonry">
		<a href="<?php the_permalink(); ?>">
			<div class="postImgHolder" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'posts-regular')?>)"></div>
			<div class="entradas__info">
				<h2 class="title title--blog"><?php the_title();?></h2>
				<?php if(is_post_type_archive('cursos')) :?>
				<div class="cursoInnerFecha cursoArchiveFecha">
					<span class="cursoInnerFecha__fecha--num"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[0]?> de</span>
					<span class="cursoInnerFecha__fecha--mes"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[1]?></span>
					<span class="cursoInnerFecha__fecha--anio"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[2]?></span>
				</div>
				<?php endif;?>
				<?php the_excerpt();?>
			</div>
		</a>
	</div>

</article><!-- #post -->
