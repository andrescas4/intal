<article id="post-<?php the_ID(); ?>" <?php post_class('entradasInternas'); ?>>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'intal' ),
				'after'  => '</div>',
			) );
		?>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
