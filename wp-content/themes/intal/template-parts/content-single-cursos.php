<div class="col-xl-6 col-lg-6 col-md-6">
<div class="row row-cursos justify-content-end">
	<div class="col-xl-7">
		<article id="post-<?php the_ID(); ?>" <?php post_class('entradasInternas entradasCursos'); ?>>
			<div class="entry-content">
			<div class="cursoInnerFecha">
				<span class="cursoInnerFecha__fecha--num"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[0]?> de</span>
				<span class="cursoInnerFecha__fecha--mes"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[1]?></span>
				<span class="cursoInnerFecha__fecha--anio"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[2]?></span>
			</div>
				<?php
					the_content();	

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'intal' ),
						'after'  => '</div>',
					) );
				?>
				<div class="profesores row">
						<?php 
							$profeID = get_post_meta(get_the_ID(), 'cursos_profesores_metabox', false)[0];
							$profeArray = array(
								'post_type' => 'profesores',
								'posts_per_page' => -1,
								'post_status' => 'publish',
								'post__in' => $profeID
							);
							$profe_query = new WP_Query( $profeArray );
						?>
						<?php if($profe_query->have_posts()) :?>
							<?php while ( $profe_query->have_posts() ): $profe_query->the_post() ;?>
								<div class="profe col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<aside class="profe--wrapper">
										<?php echo get_the_post_thumbnail(get_the_ID(), 'medium', array('class'=>'img-fluid'));?>
										<div class="profe--info">
											<h4><?php the_title();?></h4>
											<span><?php echo get_post_meta(get_the_ID(), 'profesores_info_titulo', true)?></span>
											<div class="profe__redes">
												<?php if(get_post_meta(get_the_ID(), 'profesores_facebook', true)) :?>
													<a href="<?php echo get_post_meta(get_the_ID(), 'profesores_facebook', true)?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
												<?php endif;?>
												<?php if(get_post_meta(get_the_ID(), 'profesores_twitter', true)) :?>
													<a href="<?php echo get_post_meta(get_the_ID(), 'profesores_twitter', true)?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
												<?php endif;?>
												<?php if(get_post_meta(get_the_ID(), 'profesores_linkedin', true)) :?>
													<a href="<?php echo get_post_meta(get_the_ID(), 'profesores_linkedin', true)?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
												<?php endif;?>
											</div>
										</div>
									</aside>
								</div>
								
							<?php endwhile; wp_reset_postdata();?>
						<?php endif;?>

				</div>
				<div class="metaCursos">
					<p><span>Lugar: </span><?php echo get_post_meta(get_the_ID(), 'cursos_lugar', true)?></p>
					<p><span>Horario: </span><?php echo get_post_meta(get_the_ID(), 'cursos_hora', true)?></p>
					<p><span>Precio: </span><?php echo get_post_meta(get_the_ID(), 'cursos_lugar', true)?></p>
				</div>
			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->
	</div>
</div>
</div>
<div class="col-xl-6 col-lg-6 col-md-6 b">
	<div class="row row-cursos">
		<div class="col-xl-7">
			<?php echo do_shortcode('[contact-form-7 id="119" title="Formulario cursos" html_class="col-lg-12 formCursos"]')?>	
		</div>
	</div>
</div>
