<?php 
global $post;

$cursosArg = array(
    'post_type' => 'cursos',
    'posts_per_page' => 3,
    'post_status' => 'publish'
);

$cursos = new WP_Query($cursosArg);
?>

<?php if($cursos->have_posts()) :?>
<?php while($cursos->have_posts()) : $cursos->the_post(); ?>
    <div class="col-lg-4">
        <a href="<?php the_permalink(); ?>" class="cursoLink">   
        <aside class="row curso">
                <div class="col curso__fecha">
                    <span class="curso__fecha--num"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[0]?></span>
                    <span class="curso__fecha--mes"><?php echo displayDate(get_post_meta( get_the_ID(), 'cursos_fecha', true ))[1]?></span>
                </div>
                <h2 class="col curso__titulo d-flex align-items-end"><?php the_title(); ?></h2>
        </aside>
        </a>
    </div>
<?php endwhile;?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
	<p><?php esc_html_e( 'Lo sentimos, no hay entradas para mostrar.' ); ?></p>
<?php endif; ?>