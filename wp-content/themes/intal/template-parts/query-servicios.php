<?php 
    $termsArgs = array(
        'hide_empty' => false
    );
    $terms = get_terms( 'cat_servicios', $termsArgs ); 
?>

<?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) : ?>
    <?php foreach ( $terms as $term ):?>

        <?php 
            $term_link = get_term_link( $term );
            $serviciosArg = array(
                'post_type' => 'servicios',
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'cat_servicios',
                        'field'    => 'slug',
                        'terms'    => $term->slug,
                    ),
                ),
            );
            $servicios = new WP_Query($serviciosArg);
        ?>
        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
            <aside class="menu-servicios">
                <h2 class="title title--servicios">
                        <a href="<?php echo esc_url( $term_link ); ?>"><?php echo $term->name; ?></a>
                </h2>
                <ul>
                    <?php if($servicios->have_posts()) :?>
                        <?php while($servicios->have_posts()) : $servicios->the_post(); ?>
                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                        <?php endwhile;?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </ul>
            </aside>
        </div>
    <?php endforeach;?>
<?php endif; ?>