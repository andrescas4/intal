<article <?php post_class(); ?>>
	<div class="entradas">
        <div class="postImgHolder postImgHolderProfe" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'posts-profesores')?>)"></div>
        <div class="profe--info profe--infoEmpleados">
            <h4><?php the_title();?></h4>
            <span><?php echo get_post_meta(get_the_ID(), 'profesores_info_titulo', true)?></span>
            <div class="profe__redes profe__redesEmpleados">
                <?php if(get_post_meta(get_the_ID(), 'profesores_facebook', true)) :?>
                    <a href="<?php echo get_post_meta(get_the_ID(), 'profesores_facebook', true)?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <?php endif;?>
                <?php if(get_post_meta(get_the_ID(), 'profesores_twitter', true)) :?>
                    <a href="<?php echo get_post_meta(get_the_ID(), 'profesores_twitter', true)?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <?php endif;?>
                <?php if(get_post_meta(get_the_ID(), 'profesores_linkedin', true)) :?>
                    <a href="<?php echo get_post_meta(get_the_ID(), 'profesores_linkedin', true)?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                <?php endif;?>
            </div>
        </div>
	</div>

</article><!-- #post -->