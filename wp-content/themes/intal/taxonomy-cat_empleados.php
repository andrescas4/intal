<?php get_header(); ?>
<div class="serviciosBanner d-flex justify-content-center align-items-center" style="background-image: url(<?php echo taxonomy_image(get_queried_object_id(), 'empleados_term_field')?>)">
	<h1 class="title"><?php echo taxonomy_info('cat_empleados')->name; ?></h1>
</div>

<div class="container">
	<div class="row">
		<div class="col-xl-12">
			<div class="servicioDescripcion">
				<p><?php echo taxonomy_info('cat_empleados')->description ?></p>
			</div>
		</div>
    </div>
</div>
<?php 
$custom_terms = get_terms('cat_empleados', array('childless' => true));

foreach($custom_terms as $custom_term) : ?>
<div class="equipo-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl">
                <h1 class="title title--empleados"><?php echo $custom_term->name?></h1>
            </div>
            <div class="empleados-slick">
            <div class="arrows">
                <div class="empleadosSlide">
                <?php 
                    wp_reset_query();
                    $args = array('post_type' => 'empleados',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'cat_empleados',
                                'field' => 'slug',
                                'terms' => $custom_term->slug
                            ),
                        ),
                    );

                    $loopEmpleados = new WP_Query($args);
                ?>
                <?php if($loopEmpleados->have_posts() ) :?>
                    <?php while ( $loopEmpleados->have_posts() ) : $loopEmpleados->the_post() ;?>  
                        <div class="slider">  <?php  get_template_part( 'template-parts/content', 'empleados' ); ?> </div>                       
                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php  endforeach;?>
<?php get_footer();
