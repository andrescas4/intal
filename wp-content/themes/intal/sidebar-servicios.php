<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Intal
 */

if ( ! is_active_sidebar( 'servicios' ) ) {
    return;
}
?>

<aside id="secondary" class="servicios-widget-area">
	<?php dynamic_sidebar( 'servicios' ); ?>
</aside><!-- #secondary -->
