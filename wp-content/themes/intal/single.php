<?php
get_header(); ?>
<?php global $post;?>
<div class="serviciosBanner d-flex justify-content-center align-items-center" style="background-image: url(<?php echo get_post_meta($post->ID, 'servicios_banner', true) ?>)">
	<h1 class="title"><?php   echo get_the_title($post->ID);?></h1>
</div>
<div class="container">
	<div class="row">
	
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', 'single' );
			the_post_navigation(array(
				'prev_text' => 'Anterior',
				'next_text' => 'Siguiente'
			));
		endwhile;
		?>
	
	</div>
</div>
<?php
get_footer();
