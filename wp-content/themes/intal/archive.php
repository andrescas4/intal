<?php get_header(); ?>

<div class="container">
	<div class="">
	<div class="grid">
	<div class="grid-sizer"></div>
		<?php
		if ( have_posts() ) : ?>
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile;
			the_posts_navigation();
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>
	</div>	
	</div>
</div>
<?php if(is_post_type_archive('cursos')) :?>
	<div class="bg-colorForm">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-9">
					<aside>
						<h1>Solicitud de cursos</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					</aside>
				</div>
				<div class="col-lg-6">
					<?php echo do_shortcode('[contact-form-7 id="148" title="Formulario cursos general" html_class="col-lg-12 formCursos formCursosFooter"]')?>
				</div>
			</div>
		</div>
	</div>
<?php endif;?>
<?php get_footer();
