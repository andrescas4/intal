<?php
get_header(); ?>
<?php global $post;?>
<div class="serviciosBanner d-flex justify-content-center align-items-center" style="background-image: url(<?php echo get_post_meta($post->ID, 'servicios_banner', true) ?>)">
	<h1 class="title"><?php   echo get_the_title($post->ID);?></h1>
</div>
<div class="container">
	<div class="row">
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', 'single-servicios' );
		endwhile;
		?>
	
</div>
</div>
<div class="formularioServicios--bg-color">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col col-xl-7">
				<?php echo do_shortcode('[contact-form-7 id="124" title="Formulario servicios" html_class="col-lg-12 formCursos"]');?>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
