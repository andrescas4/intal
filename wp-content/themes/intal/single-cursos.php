<?php
get_header(); ?>
<?php global $post;?>
<div class="serviciosBanner cursosBanner d-flex justify-content-center align-items-center" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID) ?>)">
	<h1 class="title"><?php   echo get_the_title($post->ID);?></h1>
</div>
<div class="container-fluid">
	<div class="row">
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', 'single-cursos' );
		endwhile;
		?>
	
</div>
</div>
<?php
get_footer();
