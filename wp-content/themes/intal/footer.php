<div class="footer footer--bg-color">
	<div class="container">
		<div class="row row-navs">
			<div class="col-xl col-lg col-md col-sm-6"><?php get_sidebar('footer1')?></div>
			<div class="col-xl col-lg col-md col-sm-6"><?php get_sidebar('footer2')?></div>
			<div class="col-xl col-lg col-md col-sm-6"><?php get_sidebar('footer3')?></div>
			<div class="col-xl col-lg col-md col-sm-6"><?php get_sidebar('footer4')?></div>
			<div class="col-xl col-lg col-md col-sm-6"><?php get_sidebar('footer5')?></div>
		</div>
		<div class="row">
			<div class="col-xl-12">
				<footer class="menu-footer">
					<?php get_sidebar('footer6')?>
				</footer>	
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>

</body>
</html>
