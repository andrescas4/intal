<?php add_action('cmb2_admin_init', 'amc_servicios_metabox');

function amc_servicios_metabox(){
    $prefix = 'servicios_';

    $cmb_servicios = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Imagen baner', 'cmb2' ),
        'context'       => 'side',
        'priority'     => 'high',
        'object_types'  => array( 'servicios', 'post')
    ) );

    $cmb_servicios->add_field(array(
        'name'       => esc_html__( 'Imágen baner', 'cmb2' ),
		'desc'       => esc_html__( 'Imagen para mostrar en el banner', 'cmb2' ),
		'id'         => $prefix . 'banner',
		'type'       => 'file'
    ));
}
?>