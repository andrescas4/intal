<?php 
add_action('cmb2_admin_init', 'amc_register_cursos_metabox');
add_action('cmb2_admin_init', 'amc_register_cursos_profesores_metabox');

function amc_register_cursos_metabox(){
    $prefix = 'cursos_';

    $cmb_cursos = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Información del curso', 'cmb2' ),
        'context'       => 'side',
        'priority'     => 'high',
		'object_types'  => array( 'cursos' ),
    ) );

    $cmb_cursos->add_field(array(
        'name'       => esc_html__( 'Valor', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese valor', 'cmb2' ),
		'id'         => $prefix . 'valor',
		'type'       => 'text'
    ));

    $cmb_cursos->add_field(array(
        'name'       => esc_html__( 'Ubicación', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese ubicación', 'cmb2' ),
		'id'         => $prefix . 'lugar',
		'type'       => 'text'
    ));

    $cmb_cursos->add_field(array(
        'name'       => esc_html__( 'Fecha', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese fecha', 'cmb2' ),
        'id'         => $prefix . 'fecha',
        'date_format' => 'd-n-Y',
		'type'       => 'text_date'
    ));

    $cmb_cursos->add_field(array(
        'name'       => esc_html__( 'Horario', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese hora del curso', 'cmb2' ),
		'id'         => $prefix . 'hora',
		'type'       => 'text'
    ));

}

function amc_register_cursos_profesores_metabox(){

    $prefix = 'cursos_profesores_';
    
    $cmb_cursos_profesores = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Información del curso', 'cmb2' ),
        'priority'     => 'high',
        'object_types'  => array( 'cursos' ),
    ) );

    $cmb_cursos_profesores->add_field( array(
        'name'    => __( 'Profesores', 'attack_sounds' ),
        'desc'    => __( 'Seleccione los profesores que daran este curso', 'attack_sounds' ),
        'id'      => $prefix.'metabox',
        'type'    => 'pw_multiselect',
        'options' => iweb_get_cmb_options_array( array( 'post_type' => 'profesores' ) ),
    ) );
}

function iweb_get_cmb_options_array( $query_args = array() ) {
	$defaults = array(
		'posts_per_page' => -1
	);
	$query = new WP_Query( array_replace_recursive( $defaults, $query_args ) );
	return wp_list_pluck( $query->get_posts(), 'post_title', 'ID' );
}
