<?php

add_action( 'cmb2_admin_init', 'yourprefix_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function yourprefix_register_theme_options_metabox() {

	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => 'amc_theme_options_page',
		'title'        => esc_html__( 'Opciones del tema', 'cmb2' ),
		'object_types' => array( 'options-page' ),

		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */

		'option_key'      => 'intal_theme_options', // The option key and admin menu page slug.
		'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'cmb2' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'cmb2' ), // The text for the options-page save button. Defaults to 'Save'.
		// 'disable_settings_errors' => true, // On settings pages (not options-general.php sub-pages), allows disabling.
		// 'message_cb'      => 'yourprefix_options_page_message_callback',
	) );
	
	$cmb_options->add_field( array(
		'name' => 'Logos y favicón',
		'desc' => 'Ingrese los logos de su empresa',
		'type' => 'title',
		'id'   => 'branding_title'
	) );

	$cmb_options->add_field( array(
		'name'    => 'Logo',
		'desc'    => 'Suba el logo de su empresa.',
		'id'      => 'img_logo',
		'type'    => 'file',
		// Optional:
		'options' => array('url' => false),
		'text'    => array('add_upload_file_text' => 'Subir logo'),
		'preview_size' => 'large',
	) );

	$cmb_options->add_field( array(
		'name'    => 'Favicón',
		'desc'    => 'Suba el favicon.',
		'id'      => 'img_favicon',
		'type'    => 'file',
		// Optional:
		'options' => array('url' => false),
		'text'    => array('add_upload_file_text' => 'Subir favicón'),
		'preview_size' => 'large',
	) );

	$cmb_options->add_field( array(
		'name' => 'Teléfono',
		'desc' => 'Ingrese teléfono de contacto',
		'type' => 'title',
		'id'   => 'tel'
	) );

	$cmb_options->add_field( array(
		'name'    => 'Teléfono',
		'desc'    => '',
		'id'      => 'telefonocontacto',
		'type'    => 'text_medium'
	) );

	/*$cmb_options->add_field( array(
		'name' => 'Social',
		'desc' => 'Ingrese las redes sociales',
		'type' => 'title',
		'id'   => 'social_title'
	) );

	$cmb_options->add_field( array(
		'name'    => 'Facebook',
		'desc'    => 'Ingrese la url de su pagina en facebook',
		'id'      => 'face_url',
		'type'    => 'text_medium'
	) );

	$cmb_options->add_field( array(
		'name'    => 'Twitter',
		'desc'    => 'Ingrese la url de su pagina en twitter',
		'id'      => 'tw_url',
		'type'    => 'text_medium'
	) );*/

}