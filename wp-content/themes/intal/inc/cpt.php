<?php 
/******************************************************* 
>>>>>> CPT Valores
*******************************************************/

function amc_cpt_valores() {

	$labels = array(
		'name'                => _x( 'Valores', 'Post Type General Name', 'intal' ),
		'singular_name'       => _x( 'Valores', 'Post Type Singular Name', 'intal' ),
		'menu_name'           => __( 'Valores', 'intal' ),
		'parent_item_colon'   => __( 'Parent Item:', 'intal' ),
		'all_items'           => __( 'Todos los Valores', 'intal' ),
		'view_item'           => __( 'Ver Valores', 'intal' ),
		'add_new_item'        => __( 'Agregar nuevo Valores', 'intal' ),
		'add_new'             => __( 'Agregar nuevo', 'intal' ),
		'edit_item'           => __( 'Editar Valores', 'intal' ),
		'update_item'         => __( 'Actualizar Valores', 'intal' ),
		'search_items'        => __( 'Buscar Valores', 'intal' ),
		'not_found'           => __( 'No se encontro plan', 'intal' ),
		'not_found_in_trash'  => __( 'No se encontro Valores en la basura', 'intal' ),
	);
	$args = array(
		'label'               => __( 'Valores', 'intal' ),
		'description'         => __( 'Valores usuarios', 'intal' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5.6644545454554, //SE PUEDE MODIFICAR ESTE NUMERO Y VERIFICAR QUE TODO ESTE BIEN DENTRO DEL ADMIN
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'valores', $args ); // PONER EL NOMBRE DEL CPT QUE QUEREMOS REGISTRAR EN MINUSCULAS

}

// Hook para iniciar este CPT
add_action( 'init', 'amc_cpt_valores', 0 );


/******************************************************* 
>>>>>> CPT certificados
*******************************************************/

function amc_cpt_certificados() {

	$labels = array(
		'name'                => _x( 'Certificados', 'Post Type General Name', 'intal' ),
		'singular_name'       => _x( 'Certificados', 'Post Type Singular Name', 'intal' ),
		'menu_name'           => __( 'Certificados', 'intal' ),
		'parent_item_colon'   => __( 'Parent Item:', 'intal' ),
		'all_items'           => __( 'Todos los Certificados', 'intal' ),
		'view_item'           => __( 'Ver Certificados', 'intal' ),
		'add_new_item'        => __( 'Agregar nuevo Certificados', 'intal' ),
		'add_new'             => __( 'Agregar nuevo', 'intal' ),
		'edit_item'           => __( 'Editar Certificados', 'intal' ),
		'update_item'         => __( 'Actualizar Certificados', 'intal' ),
		'search_items'        => __( 'Buscar Certificados', 'intal' ),
		'not_found'           => __( 'No se encontro plan', 'intal' ),
		'not_found_in_trash'  => __( 'No se encontro Certificados en la basura', 'intal' ),
	);
	$args = array(
		'label'               => __( 'Certificados', 'intal' ),
		'description'         => __( 'Certificados usuarios', 'intal' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6.6644545454554, //SE PUEDE MODIFICAR ESTE NUMERO Y VERIFICAR QUE TODO ESTE BIEN DENTRO DEL ADMIN
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'certificados', $args ); // PONER EL NOMBRE DEL CPT QUE QUEREMOS REGISTRAR EN MINUSCULAS

}

// Hook para iniciar este CPT
add_action( 'init', 'amc_cpt_certificados', 0 );

/******************************************************* 
>>>>>> CPT Suscriptores
*******************************************************/

function amc_cpt_suscriptores() {

	$labels = array(
		'name'                => _x( 'Subscriptores', 'Post Type General Name', 'intal' ),
		'singular_name'       => _x( 'Subscriptores', 'Post Type Singular Name', 'intal' ),
		'menu_name'           => __( 'Subscriptores', 'intal' ),
		'parent_item_colon'   => __( 'Parent Item:', 'intal' ),
		'all_items'           => __( 'Todos los Subscriptores', 'intal' ),
		'view_item'           => __( 'Ver Subscriptores', 'intal' ),
		'add_new_item'        => __( 'Agregar nuevo Subscriptores', 'intal' ),
		'add_new'             => __( 'Agregar nuevo', 'intal' ),
		'edit_item'           => __( 'Editar Subscriptores', 'intal' ),
		'update_item'         => __( 'Actualizar Subscriptores', 'intal' ),
		'search_items'        => __( 'Buscar Subscriptores', 'intal' ),
		'not_found'           => __( 'No se encontro plan', 'intal' ),
		'not_found_in_trash'  => __( 'No se encontro Subscriptores en la basura', 'intal' ),
	);
	$args = array(
		'label'               => __( 'Subscriptores', 'intal' ),
		'description'         => __( 'Subscriptores usuarios', 'intal' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 7.6644545454554, //SE PUEDE MODIFICAR ESTE NUMERO Y VERIFICAR QUE TODO ESTE BIEN DENTRO DEL ADMIN
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'suscriptores', $args ); // PONER EL NOMBRE DEL CPT QUE QUEREMOS REGISTRAR EN MINUSCULAS

}

// Hook para iniciar este CPT
//add_action( 'init', 'amc_cpt_suscriptores', 0 );

/******************************************************* 
>>>>>> CPT Cursos
*******************************************************/

function amc_cpt_cursos() {

	$labels = array(
		'name'                => _x( 'Cursos', 'Post Type General Name', 'intal' ),
		'singular_name'       => _x( 'Cursos', 'Post Type Singular Name', 'intal' ),
		'menu_name'           => __( 'Cursos', 'intal' ),
		'parent_item_colon'   => __( 'Parent Item:', 'intal' ),
		'all_items'           => __( 'Todos los Cursos', 'intal' ),
		'view_item'           => __( 'Ver Cursos', 'intal' ),
		'add_new_item'        => __( 'Agregar nuevo Cursos', 'intal' ),
		'add_new'             => __( 'Agregar nuevo', 'intal' ),
		'edit_item'           => __( 'Editar Cursos', 'intal' ),
		'update_item'         => __( 'Actualizar Cursos', 'intal' ),
		'search_items'        => __( 'Buscar Cursos', 'intal' ),
		'not_found'           => __( 'No se encontro plan', 'intal' ),
		'not_found_in_trash'  => __( 'No se encontro Cursos en la basura', 'intal' ),
	);
	$args = array(
		'label'               => __( 'Cursos', 'intal' ),
		'description'         => __( 'Cursos usuarios', 'intal' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8.6644545454554, //SE PUEDE MODIFICAR ESTE NUMERO Y VERIFICAR QUE TODO ESTE BIEN DENTRO DEL ADMIN
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'cursos', $args ); // PONER EL NOMBRE DEL CPT QUE QUEREMOS REGISTRAR EN MINUSCULAS

}

// Hook para iniciar este CPT
add_action( 'init', 'amc_cpt_cursos', 0 );


/******************************************************* 
>>>>>> CPT Profesores
*******************************************************/

function amc_cpt_profesores() {
	
		$labels = array(
			'name'                => _x( 'Profesores', 'Post Type General Name', 'intal' ),
			'singular_name'       => _x( 'Profesores', 'Post Type Singular Name', 'intal' ),
			'menu_name'           => __( 'Profesores', 'intal' ),
			'parent_item_colon'   => __( 'Parent Item:', 'intal' ),
			'all_items'           => __( 'Todos los Profesores', 'intal' ),
			'view_item'           => __( 'Ver Profesores', 'intal' ),
			'add_new_item'        => __( 'Agregar nuevo Profesores', 'intal' ),
			'add_new'             => __( 'Agregar nuevo', 'intal' ),
			'edit_item'           => __( 'Editar Profesores', 'intal' ),
			'update_item'         => __( 'Actualizar Profesores', 'intal' ),
			'search_items'        => __( 'Buscar Profesores', 'intal' ),
			'not_found'           => __( 'No se encontro profesor', 'intal' ),
			'not_found_in_trash'  => __( 'No se encontro Profesores en la basura', 'intal' ),
		);
		$args = array(
			'label'               => __( 'Profesores', 'intal' ),
			'description'         => __( 'Profesores usuarios', 'intal' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'thumbnail' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 10.6644545454554, //SE PUEDE MODIFICAR ESTE NUMERO Y VERIFICAR QUE TODO ESTE BIEN DENTRO DEL ADMIN
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);
		register_post_type( 'profesores', $args ); // PONER EL NOMBRE DEL CPT QUE QUEREMOS REGISTRAR EN MINUSCULAS
	
	}
	
	// Hook para iniciar este CPT
	add_action( 'init', 'amc_cpt_profesores', 0 );

/******************************************************* 
>>>>>> CPT empleados
*******************************************************/

function amc_cpt_empleados() {
	
		$labels = array(
			'name'                => _x( 'Empleados', 'Post Type General Name', 'intal' ),
			'singular_name'       => _x( 'Empleados', 'Post Type Singular Name', 'intal' ),
			'menu_name'           => __( 'Empleados', 'intal' ),
			'parent_item_colon'   => __( 'Parent Item:', 'intal' ),
			'all_items'           => __( 'Todos los Empleados', 'intal' ),
			'view_item'           => __( 'Ver Empleados', 'intal' ),
			'add_new_item'        => __( 'Agregar nuevo Empleados', 'intal' ),
			'add_new'             => __( 'Agregar nuevo', 'intal' ),
			'edit_item'           => __( 'Editar Empleados', 'intal' ),
			'update_item'         => __( 'Actualizar Empleados', 'intal' ),
			'search_items'        => __( 'Buscar Empleados', 'intal' ),
			'not_found'           => __( 'No se encontro plan', 'intal' ),
			'not_found_in_trash'  => __( 'No se encontro Empleados en la basura', 'intal' ),
		);
		$args = array(
			'label'               => __( 'Empleados', 'intal' ),
			'description'         => __( 'Empleados usuarios', 'intal' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'thumbnail' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 10.6644545454554, //SE PUEDE MODIFICAR ESTE NUMERO Y VERIFICAR QUE TODO ESTE BIEN DENTRO DEL ADMIN
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);
		register_post_type( 'empleados', $args ); // PONER EL NOMBRE DEL CPT QUE QUEREMOS REGISTRAR EN MINUSCULAS
	
	}
	
	// Hook para iniciar este CPT
	add_action( 'init', 'amc_cpt_empleados', 0 );

/******************************************************* 
>>>>>> CPT servicios
*******************************************************/

function amc_cpt_servicios() {

	$labels = array(
		'name'                => _x( 'Servicios', 'Post Type General Name', 'intal' ),
		'singular_name'       => _x( 'Servicios', 'Post Type Singular Name', 'intal' ),
		'menu_name'           => __( 'Servicios', 'intal' ),
		'parent_item_colon'   => __( 'Parent Item:', 'intal' ),
		'all_items'           => __( 'Todos los Servicios', 'intal' ),
		'view_item'           => __( 'Ver Servicios', 'intal' ),
		'add_new_item'        => __( 'Agregar nuevo Servicios', 'intal' ),
		'add_new'             => __( 'Agregar nuevo', 'intal' ),
		'edit_item'           => __( 'Editar Servicios', 'intal' ),
		'update_item'         => __( 'Actualizar Servicios', 'intal' ),
		'search_items'        => __( 'Buscar Servicios', 'intal' ),
		'not_found'           => __( 'No se encontro plan', 'intal' ),
		'not_found_in_trash'  => __( 'No se encontro Servicios en la basura', 'intal' ),
	);
	$args = array(
		'label'               => __( 'Servicios', 'intal' ),
		'description'         => __( 'Servicios usuarios', 'intal' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 9.6644545454554, //SE PUEDE MODIFICAR ESTE NUMERO Y VERIFICAR QUE TODO ESTE BIEN DENTRO DEL ADMIN
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'servicios', $args ); // PONER EL NOMBRE DEL CPT QUE QUEREMOS REGISTRAR EN MINUSCULAS

}

// Hook para iniciar este CPT
add_action( 'init', 'amc_cpt_servicios', 0 );

/******************************************************* 
>>>>>> TAXONOMIAS Servicios
*******************************************************/

function amc_tax_servicios() {

	$labels = array(
		'name'                       => _x( 'Categoría servicios', 'Taxonomy General Name', 'intal' ),
		'singular_name'              => _x( 'Categoría servicios', 'Taxonomy Singular Name', 'intal' ),
		'menu_name'                  => __( 'Categoría servicios', 'intal' ),
		'all_items'                  => __( 'Todos los Categoría servicios', 'intal' ),
		'parent_item'                => __( 'Categoria padre', 'intal' ),
		'parent_item_colon'          => __( 'Parent Item:', 'intal' ),
		'new_item_name'              => __( 'Nuevo Categoría servicios', 'intal' ),
		'add_new_item'               => __( 'Agregar nuevo Categoría servicios', 'intal' ),
		'edit_item'                  => __( 'Editar Categoría servicios', 'intal' ),
		'update_item'                => __( 'Actualizar Categoría servicios', 'intal' ),
		'separate_items_with_commas' => __( 'Separar Categoría servicios con comas', 'intal' ),
		'search_items'               => __( 'Buscar Categoría servicios', 'intal' ),
		'add_or_remove_items'        => __( 'Agregar o quitar Categoría servicios', 'intal' ),
		'choose_from_most_used'      => __( 'Escoger de las mas usadas', 'intal' ),
		'not_found'                  => __( 'No encontrada', 'intal' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' => array( 'slug' => 'servicios-intal' ),
	);
	register_taxonomy( 'cat_servicios', array( 'servicios' ), $args ); // PONER EL NOMBRE DE LA TAXONOMIA QUE QUEREMOS REGISTRAR EN MINUSCULAS Y EL NOMBRE DEL CPT AL QUE LA QUEREMOS ASOCIAR

}

// Hook para iniciar las taxonomias
add_action( 'init', 'amc_tax_servicios', 0 );


/******************************************************* 
>>>>>> TAXONOMIAS Empleados
*******************************************************/

function amc_tax_empleados() {
	
		$labels = array(
			'name'                       => _x( 'Categoría empleados', 'Taxonomy General Name', 'intal' ),
			'singular_name'              => _x( 'Categoría empleados', 'Taxonomy Singular Name', 'intal' ),
			'menu_name'                  => __( 'Categoría empleados', 'intal' ),
			'all_items'                  => __( 'Todos los Categoría empleados', 'intal' ),
			'parent_item'                => __( 'Categoria padre', 'intal' ),
			'parent_item_colon'          => __( 'Parent Item:', 'intal' ),
			'new_item_name'              => __( 'Nuevo Categoría empleados', 'intal' ),
			'add_new_item'               => __( 'Agregar nuevo Categoría empleados', 'intal' ),
			'edit_item'                  => __( 'Editar Categoría empleados', 'intal' ),
			'update_item'                => __( 'Actualizar Categoría empleados', 'intal' ),
			'separate_items_with_commas' => __( 'Separar Categoría empleados con comas', 'intal' ),
			'search_items'               => __( 'Buscar Categoría empleados', 'intal' ),
			'add_or_remove_items'        => __( 'Agregar o quitar Categoría empleados', 'intal' ),
			'choose_from_most_used'      => __( 'Escoger de las mas usadas', 'intal' ),
			'not_found'                  => __( 'No encontrada', 'intal' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite' => array( 'slug' => 'equipo', 'with_front' => false ),
		);
		register_taxonomy( 'cat_empleados', array( 'empleados' ), $args ); // PONER EL NOMBRE DE LA TAXONOMIA QUE QUEREMOS REGISTRAR EN MINUSCULAS Y EL NOMBRE DEL CPT AL QUE LA QUEREMOS ASOCIAR
	
	}
	
	// Hook para iniciar las taxonomias
	add_action( 'init', 'amc_tax_empleados', 0 );


