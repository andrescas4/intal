<?php add_action('cmb2_admin_init', 'amc_shared_metabox');

function amc_shared_metabox(){
    $prefix = 'shared_';

    $cmb_shared = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Destacar entrada', 'cmb2' ),
        'context'       => 'side',
        'priority'     => 'high',
		'object_types'  => array( 'cursos', 'servicos', 'post' ),
    ) );

    $cmb_shared->add_field(array(
        'name'       => esc_html__( 'Destacar', 'cmb2' ),
		'desc'       => esc_html__( 'Seleccione si desea destacar esta entrada', 'cmb2' ),
		'id'         => $prefix . 'meta',
		'type'       => 'checkbox'
    ));


}
