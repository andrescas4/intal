<?php 
add_action('cmb2_admin_init', 'amc_register_profesores_metabox');
add_action('cmb2_admin_init', 'amc_register_profesores_info_metabox');

/* CMB redes sociales */ 
function amc_register_profesores_metabox(){
    $prefix = 'profesores_';

    $cmb_profesores = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Redes sociales', 'cmb2' ),
		'object_types'  => array( 'profesores', 'empleados' ),
    ) );
    
    $cmb_profesores->add_field(array(
        'name'       => esc_html__( 'Facebook', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese url Facebook', 'cmb2' ),
		'id'         => $prefix . 'facebook',
		'type'       => 'text'
    ));

    $cmb_profesores->add_field(array(
        'name'       => esc_html__( 'Twitter', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese url twitter', 'cmb2' ),
		'id'         => $prefix . 'twitter',
		'type'       => 'text'
    ));

    $cmb_profesores->add_field(array(
        'name'       => esc_html__( 'Linkedin', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese url Linkedin', 'cmb2' ),
		'id'         => $prefix . 'linkedin',
		'type'       => 'text'
    ));
}

/* CMB Título profesional */
function amc_register_profesores_info_metabox(){
    $prefix = 'profesores_info_';

    $cmb_profesores_info = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Título profesional', 'cmb2' ),
		'object_types'  => array( 'profesores', 'empleados' ),
    ) );

    $cmb_profesores_info->add_field(array(
        'name'       => esc_html__( 'Título profesional', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese título profesional', 'cmb2' ),
		'id'         => $prefix . 'titulo',
		'type'       => 'text'
    ));
}

?>