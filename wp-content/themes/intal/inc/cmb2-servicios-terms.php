<?php 
add_action('cmb2_admin_init', 'amc_register_servicios_term_metabox');

function amc_register_servicios_term_metabox() {
	$prefix = 'servicios_term_';

	/**
	 * Metabox to add fields to categories and tags
	 */
	$cmb_term = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => esc_html__( 'Category Metabox', 'cmb2' ),
		'object_types'     => array( 'term' ),
		'taxonomies'       => array( 'cat_servicios'),
	) );

	$cmb_term->add_field( array(
		'name' => esc_html__( 'Agregar imagen', 'cmb2' ),
		'desc' => esc_html__( 'Agrege imagen a la categoría', 'cmb2' ),
		'id'   => $prefix . 'field',
		'type' => 'file',
	) );

}