<?php add_action('cmb2_admin_init', 'amc_page_metabox');

function amc_page_metabox(){
    $prefix = 'page_';

    $cmb_page = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
        'title'         => esc_html__( 'Listado', 'cmb2' ),
        'context'       => 'normal',
        'priority'     => 'high',
        'object_types'  => array( 'page'),
        'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/special-page.php' )
    ) );

    $cmb_page->add_field(array(
        'name'       => esc_html__( 'Textos extras', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese listado extra', 'cmb2' ),
		'id'         => $prefix . 'listado',
		'type'       => 'wysiwyg'
    ));
}
?>