<?php 
add_action('cmb2_admin_init', 'amc_register_home_proximos_cursos_metabox');
add_action('cmb2_admin_init', 'amc_register_home_valores_metabox');


function amc_register_home_proximos_cursos_metabox(){
    $prefix = 'home_proximos_cursos_';

    $cmb_home = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Opciones sección próximos cursos', 'cmb2' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/home.php' )
    ) );
    
    $cmb_home->add_field(array(
        'name'       => esc_html__( 'Título próximos cursos', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese título para la sección cursos', 'cmb2' ),
		'id'         => $prefix . 'titulo',
		'type'       => 'text'
    ));

}

function amc_register_home_valores_metabox(){
    $prefix = 'home_valores_';

    $cmb_home = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Opciones sección valores', 'cmb2' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/home.php' )
    ) );
    
    $cmb_home->add_field(array(
        'name'       => esc_html__( 'Título valores', 'cmb2' ),
		'desc'       => esc_html__( 'Ingrese título para la sección valores', 'cmb2' ),
		'id'         => $prefix . 'titulo',
		'type'       => 'text'
    ));

}