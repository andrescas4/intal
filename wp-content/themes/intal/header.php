<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Open+Sans:300,400,600,700|Roboto" rel="stylesheet">
	<script src="https://use.fontawesome.com/ba7feae64a.js"></script>
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="generalWrapper">
	<div class="container">
		<div class="row menuInLine">
			<div class="col col-xl-3 col-sm-9 col-xs-9 justify-content-start">

				<a href="<?php echo home_url('/')?>" class="logo"><img src="<?php echo get_option( 'intal_theme_options' )['img_logo'];?>" alt="" class="img img-fluid"></a>
			</div>
			<div class="col col-xl-9  col-sm-3 col-xs-3">
				<button id="btnMenuRes" class="hamburger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<?php wp_nav_menu( array(
					'menu' => 'Menú cabezote',
					'menu_class' => 'd-flex justify-content-end menuInLine--menu-header',
					'container' => false
				) );?>
				<nav id="menuRes">
				<?php wp_nav_menu( array(
					'menu' => 'Menú responsive',
					'container' => false
				) );?>
				</nav>
			</div>
		</div><!-- fin row -->
	</div>
	<div class="menuppal menuppal--bg-color">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="d-flex justify-content-end">
						<?php wp_nav_menu( array(
							'menu' => 'Menú ppal',
							'menu_class' => 'd-flex justify-content-end menuInLine menuInLine--menu-secundario',
							'container' => false
						) );?>
						
						<div class="searchMain">
							<a class="s" data-content='<form action="/" class="searchMain--form"><input type="search" name="s" placeholder="Buscar..."></form>' data-placement="left" data-toggle="popover"  data-html="true"><i class="fa fa-search" aria-hidden="true" ></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="sliderCont">
<?php get_template_part('template-parts/royal', 'slider'); ?>
</div>