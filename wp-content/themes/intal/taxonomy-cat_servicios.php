<?php get_header(); ?>
<div class="serviciosBanner d-flex justify-content-center align-items-center" style="background-image: url(<?php echo taxonomy_image(get_queried_object_id(), 'servicios_term_field')?>)">
	<h1 class="title"><?php echo taxonomy_info('cat_servicios')->name; ?></h1>
</div>

<div class="container">
	<div class="row">
		<div class="col-xl-12">
			<div class="servicioDescripcion">
				<p><?php echo taxonomy_info('cat_servicios')->description ?></p>
			</div>
		</div>
		<div class="col-xl-12">
			<div class="row">
				<?php
				if ( have_posts() ) : ?>
					<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', 'servicios' );
					endwhile;
					the_posts_navigation();
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif; ?>
			</div>
		</div>
    </div>
</div>

<?php get_footer();
