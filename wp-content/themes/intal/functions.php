<?php
/**
 * Intal functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Intal
 */

if ( ! function_exists( 'intal_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function intal_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Intal, use a find and replace
		 * to change 'intal' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'intal', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size('posts-thumbs', 350, 202, true);
		add_image_size('posts-medium', 700, 542, true);
		add_image_size('posts-regular', 630, 152, true);
		add_image_size('posts-profesores', 480, 444, true);

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'intal' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'intal_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'intal_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function intal_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'intal_content_width', 640 );
}
add_action( 'after_setup_theme', 'intal_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function intal_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'intal' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Pie de página 1', 'intal' ),
		'id'            => 'footer1',
		'description'   => esc_html__( 'Agregue contenidos acá.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Pie de página 2', 'intal' ),
		'id'            => 'footer2',
		'description'   => esc_html__( 'Agregue contenidos acá.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Pie de página 4', 'intal' ),
		'id'            => 'footer4',
		'description'   => esc_html__( 'Agregue contenidos acá.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Pie de página 4', 'intal' ),
		'id'            => 'footer4',
		'description'   => esc_html__( 'Agregue contenidos acá.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Pie de página 5', 'intal' ),
		'id'            => 'footer5',
		'description'   => esc_html__( 'Agregue contenidos acá.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Pie de página 6', 'intal' ),
		'id'            => 'footer6',
		'description'   => esc_html__( 'Agregue contenidos acá.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Servicios', 'intal' ),
		'id'            => 'servicios',
		'description'   => esc_html__( 'Agregue contenidos acá.', 'intal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'intal_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function intal_scripts() {
	wp_enqueue_style( 'intal-style', get_stylesheet_uri() );
	wp_enqueue_style( 'intal-site-styles', get_template_directory_uri(). '/style/style.css' );

	wp_enqueue_script( 'intal-isotope',  'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array(), '', true );
	wp_enqueue_script( 'intal-imagesLoad', get_template_directory_uri() . '/scripts/imagesLoaded.js', array(), '', true );
	wp_enqueue_script( 'intal-popper', get_template_directory_uri() . '/scripts/popper.js', array(), '', true );
	wp_enqueue_script( 'intal-scripts', get_template_directory_uri() . '/scripts/scripts.js', array(), '', true );
	
	
	
	//wp_enqueue_script( 'intal-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'intal_scripts' );

function wpshout_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'posts-thumbs' => __( 'Posts thumbnails' ),
		) );
}
	
add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );

/**
 * Display taxonomy title
 */
function taxonomy_title($tax_name){
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( $tax_name ) );
	return $term->name;
}
/**
 * Display taxonomy image
 */
function taxonomy_image($id, $name){
	return $term_image = get_term_meta( $id, $name, true);
}
/**
 * Display taxonomy description
 */
function taxonomy_description($id){
	return $term_image = get_term_meta( $id, 'servicios_term_description', true);
}


function taxonomy_info($tax){
	$taxonomy = $tax;
	$queried_term = get_query_var($taxonomy);
	$term = get_term_by( 'slug', $queried_term, $taxonomy );
	return $term;
}
/**
 * Display date in special format home
 */
function displayDate($date){
	$months = array('Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$fecha = explode('-', $date);

	return array($fecha[0], $months[$fecha[1]-1], $fecha[2]);
}
function is_important($id){
	return get_post_meta($id, 'shared_meta', true);
}

function custom_menu_item ( $items, $args ) {
	$tel = get_option( 'intal_theme_options' )['telefonocontacto'];
	if ( $args->menu == 'Menú cabezote') {
		$items .= '<li><span>'.$tel.'</span></li>';
		$items .= '<li><a href="#">Pagar</a></li>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'custom_menu_item', 10, 2 );

/**
 * Custom template tags for this theme.
 */
function wp_trim_all_excerpt($text) {
	// Creates an excerpt if needed; and shortens the manual excerpt as well
	global $post;
	  $raw_excerpt = $text;
	  if ( '' == $text ) {
		$text = get_the_content('');
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
	  }
	 
	$text = strip_tags($text);
	$excerpt_length = apply_filters('excerpt_length', 18);
	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	$text = wp_trim_words( $text, $excerpt_length, $excerpt_more ); //since wp3.3
	/*$words = explode(' ', $text, $excerpt_length + 1);
	  if (count($words)> $excerpt_length) {
		array_pop($words);
		$text = implode(' ', $words);
		$text = $text . $excerpt_more;
	  } else {
		$text = implode(' ', $words);
	  }
	return $text;*/
	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt); //since wp3.3
	}
	 
	remove_filter('get_the_excerpt', 'wp_trim_excerpt');
	add_filter('get_the_excerpt', 'wp_trim_all_excerpt');


	add_filter( 'wp_calculate_image_srcset_meta', '__return_empty_array' );
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Adding CMB2
 */
//require get_template_directory() . '/inc/cmb2.php';
require get_template_directory() . '/inc/cmb2-profesores.php';
require get_template_directory() . '/inc/cmb2-options-theme.php';
require get_template_directory() . '/inc/cmb2-cursos.php';
require get_template_directory() . '/inc/cmb2-home.php';
require get_template_directory() . '/inc/cmb2-servicios-terms.php';
require get_template_directory() . '/inc/cmb2-empleados-terms.php';
require get_template_directory() . '/inc/cmb2-shared-fields.php';
require get_template_directory() . '/inc/cmb2-pages.php';
require get_template_directory() . '/inc/cmb2-servicios.php';

/**
 * Adding CMB2
 */
require get_template_directory() . '/inc/cpt.php';


