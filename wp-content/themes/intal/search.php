<?php get_header(); ?>

	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<?php
				if ( have_posts() ) : ?>

					<header class="search-header">
						<h1 class="title"><?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Resultados para: %s', 'intal' ), '<span>' . get_search_query() . '</span>' );
						?></h1>
					</header><!-- .page-header -->
					<ul class="list-group">
						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'search' );

						endwhile;

						the_posts_navigation();

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
					</ul>
			</div>
		</div><!-- #main -->
	</div><!-- #primary -->

<?php get_footer() ?>
